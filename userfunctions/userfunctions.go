/*
userFunctions.go contains RegisterUserSmartContract and implements the following functions
*queryUser (Args:EmailId)
*registerUser (Args: SPublicKey,EPublicKey,EmailId,Metadata-optional)
*queryAllUsers (Args:nil)
*changeUserDetails (Args: SPublicKey,EPublicKey,EmailId,Metadata-optional) cannot change emailId as of now
*/
package userfunctions

import (
	"bytes"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	b64 "encoding/base64"
	"encoding/json"
	"encoding/pem"
	"fmt"

	"crypto"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

// Define the Smart Contract structure
type RegisterUserSmartContract struct {
}

// Define the User structure, with 4 properties.  Structure tags are used by encoding/json library
type User struct {
	SPublicKey       string `json:"sPublicKey"`
	EPublicKey       string `json:"ePublicKey"`
	EmailId          string `json:"emailId"`
	IdentityFileHash string `json:"identityFileHash,omitempty"`
	Metadata         string `json:"metadata"`
}

/*
 * The Init method is called when the Smart Contract "userFunctions" is instantiated by the blockchain network
 */
func (R *RegisterUserSmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

/*
 * The Invoke method is called as a result of an application request to run the Smart Contract "userFunctions.go"
 * The calling application program has also specified the particular smart contract function to be called, with arguments
 */
func (R *RegisterUserSmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()
	// Route to the appropriate handler function to interact with the ledger appropriately
	if function == "queryUser" {
		return R.queryUser(APIstub, args)
	} else if function == "registerUser" {
		return R.registerUser(APIstub, args)
	} else if function == "queryAllUsers" {
		return R.queryAllUsers(APIstub)
	} else if function == "updateIdentityFile" {
		return R.updateIdentityFile(APIstub, args)
	}

	return shim.Error(fmt.Sprint("Invalid Smart Contract function name:", function))
}

func (R *RegisterUserSmartContract) queryUser(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	userAsBytes, err := APIstub.GetState(args[0])
	if err != nil {
		return shim.Error("Error in GetState!")
	}
	if userAsBytes == nil {
		return shim.Error("User doesn't exist!")
	}
	return shim.Success(userAsBytes)
}
func (R *RegisterUserSmartContract) registerUser(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}
	var user User
	data := args[0]
	sig := args[1]
	json.Unmarshal([]byte(data), &user)
	missing := ""
	if user.EmailId == "" {
		missing += " emailId"
	}
	if user.SPublicKey == "" {
		missing += " sPublicKey"
	}
	if user.EPublicKey == "" {
		missing += " ePublicKey"
	}
	if missing != "" {
		return shim.Error("Missing Required Fields:" + missing)
	}
	err := verifySignatureObject(APIstub, user, sig, data)
	if err != nil {
		return shim.Error("Signature Verification Failed: " + err.Error())
	}
	userAsBytes, err := APIstub.GetState(user.EmailId)
	if err != nil {
		return shim.Error("Error in GetState!: " + err.Error())
	}
	if userAsBytes != nil {
		return shim.Error("User already exists!")
	}
	userAsBytes, err = json.Marshal(user)
	if err != nil {
		shim.Error("Unable to Marshal json!: " + err.Error())
	}
	err = APIstub.PutState(user.EmailId, userAsBytes)
	if err != nil {
		shim.Error("Error in PutState!: " + err.Error())
	}
	return shim.Success(userAsBytes)
}

func (R *RegisterUserSmartContract) queryAllUsers(APIstub shim.ChaincodeStubInterface) sc.Response {
	startKey := ""
	endKey := ""
	resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")
	return shim.Success(buffer.Bytes())
}

func (R *RegisterUserSmartContract) updateIdentityFile(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}
	var req, user User
	data := args[0]
	sig := args[1]
	json.Unmarshal([]byte(data), &req)
	err := verifySignatureEmail(APIstub, req.EmailId, sig, data)
	if err != nil {
		shim.Error(err.Error())
	}
	userAsBytes, err := APIstub.GetState(req.EmailId)
	if err != nil {
		shim.Error(err.Error())
	}
	err = json.Unmarshal(userAsBytes, &user)
	if err != nil {
		shim.Error("Error in unmarshal!")
	}
	user.IdentityFileHash = req.IdentityFileHash
	userAsBytes, _ = json.Marshal(user)
	APIstub.PutState(req.EmailId, userAsBytes)
	return shim.Success(userAsBytes)
}

/* verifySignatureEmail: verify if the message is signed by user with ownerEmailID */
func verifySignatureEmail(APIstub shim.ChaincodeStubInterface, ownerEmailID string, signature string, message string) (err error) {
	userAsBytes, err := APIstub.GetState(ownerEmailID)
	if err != nil {
		return
	}
	var user User
	err = json.Unmarshal([]byte(userAsBytes), &user)
	if err != nil {
		return fmt.Errorf("Unmarshalling error: %s", err)
	}

	return verifySignatureObject(APIstub, user, signature, message)
}

func verifySignatureObject(APIstub shim.ChaincodeStubInterface, user User, signature string, message string) (err error) {
	block, _ := pem.Decode([]byte(user.SPublicKey))
	if block == nil {
		return fmt.Errorf("failed to decode PEM block containing public key")
	}
	pubKey, parseErr := x509.ParsePKIXPublicKey(block.Bytes)
	if parseErr != nil {
		return fmt.Errorf("Failed to parse public key: %s", parseErr)
	}
	rsaPubKey := pubKey.(*rsa.PublicKey)
	h := sha256.New()
	h.Write([]byte(message))
	digest := h.Sum(nil)
	decodedSignatureBytes, _ := b64.StdEncoding.DecodeString(signature)
	verifyErr := rsa.VerifyPKCS1v15(rsaPubKey, crypto.SHA256, digest, decodedSignatureBytes)
	return verifyErr
}
